## Synopsis

Thaddeus is a Web front-end a) for your existing image library - you scan your existing pictures and display them on a nice web interface (images not matching the required criteria are backup before any modifications are applied, which keeps file duplication to a minimum, b) manage new photos if you wish to do so through an admin interface allowing you to upload to a chosen location an display on the web interface.
It allows to add comments to your pictures as well as organizing them into photoalbums.
Thaddeus is built on Django 1.5 running on python 2.7 and requires the PIL image module.

## Code Example

From the web interface:
- Run Batch: Load/Reload all photos on the MEDIA_ROOT patch defined in settings.py into the default photoalbum
- Admin: Enter the admin interface using the superuser credentials to edit photos and photoalbums metadata (name, comments...)
Development use: If you have made a mistake and want to reset things, append "/flush" at the end of your root URL (as in for example http://localhost:8000/flush"). This will delete all records from the database and all .thumbnails files in the mdeia library, leaving the original files untouched. 

## Motivation

I just couldn't find an image gallery that would allow me to scan my files on my network folder. Files could only be added to the web interface by being uploaded one by one through the web interface, which I don't have the time or the inclination for. Disclaimer, this is my first python/Django project. Feedback welcome!

## Installation

- Create a new django app called thaddeus 
- under <DjangoAppFolder>/thaddeus, replace the thaddeus folder (as in <DjangoAppFolder>/thaddeus/thaddeus) with the one in the zip file 
- Update paths in settings.py to your own machine (library paths as well as URL paths, please note that at the moment, it should only be run on an INTERNAL network. The functionality to delete photos from disk for example is not password protected.
- install the PIL image manipulation module (http://www.pythonware.com/products/pil/) 
- Inside <DjangoAppFolder>/thaddeus, run 1) python manage.py sql thaddeus, 2) python manage.py syncdb, 3) python manage.py runserver

Please note that there is nothing in place regarding security, anybody can access the web interface and delete pictures on you so not for use outside your LAN. Also, SQLITE is so far the only back-end database system supported.

#API Reference

None so far

## Tests

3 unit tests: check the front end index page displays OK, run the load functionality with a sample, remove file from library with a sample.
From the thaddeus folder, run "python manage.py test thaddeus"

## Contributors

Contact mcornille@gmail.com

## License

BSD 3-Clause License (Revised) - see http://www.tldrlegal.com/l/BSD3 for details. Do what you want (credit would be nice all the same) as long as you don't charge back for your work if based on this.