#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
import sqlite3 as lite
from django.conf import settings
import sys, os
from PIL import Image
from optparse import make_option

class Command(BaseCommand):
    args = '<path,action,photo_id>'
    help = 'Manipulate a single pic - action options are remove, delete, rotate, rotateleft'

    def handle(self, *args, **options):
        path = args[0]
        action = args[1]
        photo_id = args[2]
            
        con = None
    
        try:
            
            pid = int(photo_id)
            PATH = path
            
            # Create stub for DB connection, reading DB name from settings.py config file
            con = lite.connect(settings.DATABASES['default']['NAME'])
            
            cur = con.cursor()
            
            prepend = ''
            
            if action == 'simulate':
                table = 'thaddeus_photo'
            else:
                table = 'thaddeus_photo'
            
            cur.execute("SELECT thumb FROM " + table + " WHERE id = ?", (pid,))
            rows = cur.fetchone()
            for row in rows:
                tmb = row
            #thumbnail = PATH+'/'+tmb
            thumbnail = PATH+tmb
            
            #cur.execute("SELECT image FROM thaddeus_photo WHERE id = ?", (pid,))
            cur.execute("SELECT image FROM " + table + " WHERE id = ?", (pid,))
            rows2 = cur.fetchone()
            for row2 in rows2:
                tmb2 = row2
            #image = PATH+'/'+tmb2
            image = PATH+tmb2
            
            if action == 'rotate' or action == 'rotateleft':
                angle = 90
                if action == 'rotate':
                    angle = 270                 
            
                # Rotate thumbnail
                im = Image.open(thumbnail)  
                im.rotate(angle).save(thumbnail,'JPEG')

                # Rotate image
                im2 = Image.open(image)                
                im2.rotate(angle).save(image,'JPEG')
            
            if action == 'remove' or action == 'delete':
                # Delete thumbnail only from filesystem, we leave the pic on filesystem untouched
                os.remove(thumbnail)
                
                # Delete from database
                #cur.execute("DELETE FROM thaddeus_photo WHERE id = ?", (pid,))
                cur.execute("DELETE FROM " + table + " WHERE id = ?", (pid,))
                con.commit()
                
            if action == 'delete':
                # Delete image from filesystem
                os.remove(image)
            
        # Error Handling            
        except lite.Error, e:
            
            print "Error %s:" % e.args[0]
            sys.exit(1)

        # Always close DB connection    
        finally:
            
            if con:
                con.close()