#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import datetime, os, sys, glob
from PIL import Image
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import fnmatch

class Command(BaseCommand):
    args = '<path optional arg>'
    help = 'Scans defined directories for common image format to auto populate the database'

    def handle(self, *args, **options):

        con = None

        try:
            # Create empty lists
            photoinserts = []
            photoalbuminserts = []
            
            # PIL Supported common image file extensions
            file_ext = {
                'tiff': ('tif','tiff'),
                'jpeg': ('jpeg','jpg','jif','jfif'),
                'png': ('png',),
                'bmp': ('bmp',),
                'gif': ('gif',)
            }
            
            # Create stub for DB connection, reading DB name from settings.py config file
            con = lite.connect(settings.DATABASES['default']['NAME'])
            
            #if path is defined as an argument
            try:
                aw = {
                    'default': {
                        'PATH': args[0],
                        'T_SIZE_HEIGHT': 200, #Size of thumbnail in pixels
                        'T_SIZE_WIDTH': 150,
                        'MAX_SIZE_HEIGHT': 800, #Max size of photos in pixels, if any bigger, will be resized
                        'MAX_SIZE_WIDTH': 600,
                        'ENFORCE_RESIZE': True, #Resize all photos to MAX_SIZE regardless of whether bigger or smaller
                        'KEEP_ORIGINAL': True #In case of a resize, original photos will be renamed to XXX_ORIGINAL.XXX
                        }
                    }
            except:
                aw = settings.FILE_STORAGE
            
            #Reading some globals from settings.py defining image handling
            #For each path
            for az in aw.keys():
                PATH = settings.FILE_STORAGE[az]['PATH']
                if len(settings.FILE_STORAGE) > 1:
                    #More than than 1 FILE_STORAGE path defined
                    mypath = PATH.replace("\\", "/")
                    myfolder = mypath[mypath.rfind("/"):]
                    if len(myfolder) == 1:
                        #Got the final slash in path, not the actual folder
                        mypath = mypath[:len(mypath)-1]
                        myfolder = mypath[mypath.rfind("/")+1:]
                else:
                    myfolder = ''
                if settings.FILE_STORAGE[az].has_key('T_SIZE_HEIGHT'):
                    T_SIZE_HEIGHT = settings.FILE_STORAGE[az]['T_SIZE_HEIGHT']
                else:
                    T_SIZE_HEIGHT = settings.FILE_STORAGE['default']['T_SIZE_HEIGHT']
                if settings.FILE_STORAGE[az].has_key('T_SIZE_WIDTH'):
                    T_SIZE_WIDTH = settings.FILE_STORAGE[az]['T_SIZE_WIDTH']
                else:
                    T_SIZE_WIDTH = settings.FILE_STORAGE['default']['T_SIZE_WIDTH']
                if settings.FILE_STORAGE[az].has_key('MAX_SIZE_HEIGHT'):
                    MAX_SIZE_HEIGHT = settings.FILE_STORAGE[az]['MAX_SIZE_HEIGHT']
                else:
                    MAX_SIZE_HEIGHT = settings.FILE_STORAGE['default']['MAX_SIZE_HEIGHT'] 
                if settings.FILE_STORAGE[az].has_key('MAX_SIZE_WIDTH'):
                    MAX_SIZE_WIDTH = settings.FILE_STORAGE[az]['MAX_SIZE_WIDTH']
                else:
                    MAX_SIZE_WIDTH = settings.FILE_STORAGE['default']['MAX_SIZE_WIDTH'] 
                if settings.FILE_STORAGE[az].has_key('ENFORCE_RESIZE'):
                    ENFORCE_SIZE = settings.FILE_STORAGE[az]['ENFORCE_RESIZE']
                else:
                    ENFORCE_SIZE = settings.FILE_STORAGE['default']['ENFORCE_RESIZE'] 
                if settings.FILE_STORAGE[az].has_key('KEEP_ORIGINAL'):
                    KEEP_ORIGINAL = settings.FILE_STORAGE[az]['KEEP_ORIGINAL']
                else:
                    KEEP_ORIGINAL = settings.FILE_STORAGE['default']['KEEP_ORIGINAL']
                
                #Read folder structure into formatted list of dictionaries {photoalbum names:path} called allalbum
                allalbum = []
                # Use 'os.walk' module to read filesystem
                for root, directories, files in os.walk(PATH):
                    if directories:
                        #We have subdirectories
                        for i in directories:
                            if root == PATH and len(os.listdir(os.path.join(root, i))) <> 0:
                                # at root level, append to result list a dictionary {directory name as photoalbum name:path}
                                if myfolder == '':
                                    allalbum.append({i:os.path.normpath(os.path.join(root, i))})
                                else:
                                    allalbum.append({myfolder+'-'+i:os.path.normpath(os.path.join(root, i))})
                            elif len(files) <> 0 or len(os.listdir(os.path.join(root, i))) <> 0:
                                # for any other sublevels, find first reference of parent path
                                # in order to create photoalbum name as directory<depth1>-directory<depth2>-directory<depth3>-...
                                for j in allalbum:    
                                    for k,v in j.iteritems():
                                        if os.path.relpath(root, os.path.normpath(v)) == '.':
                                            if myfolder in k or myfolder == '':
                                                allalbum.append({k+'-'+i:os.path.normpath(os.path.join(root, i))})
                                            else:
                                                allalbum.append({myfolder+'-'+k+'-'+i:os.path.normpath(os.path.join(root, i))})
                                        else:
                                            pass
                    elif len(settings.FILE_STORAGE) > 1 and root == PATH:
                        allalbum.append({myfolder:root})
                    else:
                        # only files at the root, will be captured by default photoalbum
                        pass

                # Prepare INSERT list for the photoalbum table - title,summary,coverpic_id
                for j in allalbum:    
                    for k,v in j.iteritems():
                        tmp2 = (k,'Added on '+str(datetime.date.today()), '')
                        p2 = photoalbuminserts.append(tmp2)               
                
                #For each file extension
                for ap in file_ext.keys():
                    for am in file_ext[ap]:
                        matches = []
                        tmp_ext = am
                        tmp_fmt = ap
                        # Use 'os.walk' module to read filesystem
                        for root, dirnames, filenames in os.walk(PATH):
                            for filename in fnmatch.filter(filenames, '*.'+tmp_ext):
                                aw = os.path.join(root, filename)
                                cw = aw[0:len(aw)-len('.'+tmp_ext)]
                                bw = cw + ".thumbnail"
                                if os.path.isfile(bw):
                                    pass
                                    #We will assume that if there is a thumbnail, then the file has been indexed already
                                else:                             
                                    matches.append(aw)
                                    
                        # We now have a list of images to be processed
                        for a in matches:
                            # Set the thumbnail size and image size.
                            t_size = (T_SIZE_HEIGHT, T_SIZE_WIDTH)
                            max_size = (MAX_SIZE_HEIGHT, MAX_SIZE_WIDTH)
                            # Open the image that was uploaded.
                            im = Image.open(a)
                            c = a[0:len(a)-len('.'+tmp_ext)]
                            # Compare the image size against the preferred size. If different, the image will be resized.
                            if im.size <> max_size:
                                try:
                                    #Backup image XXX.jpg in original format as XXX.XXX_ORIGINAL before resizing
                                    im.save(a+"_ORIGINAL", tmp_fmt)
                                    # First, try using 'thumbnail', instead of 'resize', keeps the aspect ratio of the image.
                                    resize = im.thumbnail(max_size)
                                    resize.save(a, str(tmp_fmt))
                                except:
                                    # 'thumbnail' doesn't work, try resize
                                    resize = im.resize(max_size)
                                    resize.save(a, str(tmp_fmt)) 
                            # Create the thumbnail the same way
                            try:
                                im.thumbnail(t_size)
                                im.save(c + ".thumbnail", "JPEG")
                            except:
                                im.resize(t_size)
                                im.save(c + ".thumbnail", "JPEG")
                            #Build list of list of inserts for the database
                            d = c[c.rfind('media')+6:len(c)]
                            # Parse and replace Windows path
                            d = d.replace("\\", "/")
                            de = d[d.rfind("/")+1:]
                            # find relevant photoalbum
                            albumname = 'default'
                            for j in allalbum:    
                                for k,v in j.iteritems():
                                    (filepath, filename) = os.path.split(a)
                                    #print filepath
                                    if os.path.relpath(v, os.path.normpath(filepath)) == '.':
                                        albumname = k
                            tmp = (de, 'Added on '+str(datetime.date.today())+' by batch', "batch", datetime.datetime.now(), d+"."+tmp_ext, d+".thumbnail",albumname)
                            p = photoinserts.append(tmp)

            with con:    
                
                cur = con.cursor()
                #INSERT "default" photoalbum in case we have images at the root
                cur.execute("INSERT INTO thaddeus_photoalbum (title,summary,coverpic_id) VALUES(?, ?, ?)", ('default','placeholder',''))
                con.commit()
                #INSERT photoalbums
                for bb in photoalbuminserts:
                    cur.execute("INSERT INTO thaddeus_photoalbum (title,summary,coverpic_id) VALUES(?, ?, ?)", bb)
                    con.commit()
                #INSERT photos
                for b in photoinserts:
                    b1 = b[0:len(b)-1]
                    b2 = b[1:]
                    #print b2
                    for b3 in b2:
                        b3 = b3
                    # Might be able to do it in one go using a subquery but can't get it to work
                    cur.execute("SELECT id from thaddeus_photoalbum WHERE title = ? LIMIT 1", [b3])
                    rows = cur.fetchone()
                    if rows:
                        for t in rows:
                                t = t
                    else:
                        t = 1
                    b4 = b1 + (t,)
                    cur.execute("INSERT INTO thaddeus_photo (title,summary,summary_html,date,image,thumb,photoalbum_id) VALUES(?, ?, ?, ?, ?, ?, ?)", b4)
                con.commit()
                #UPDATE photoalbums thumbnail to random pic within photoalbum if still set to default 
                cur.execute("SELECT id FROM thaddeus_photoalbum WHERE coverpic_id = ''")
                rs = cur.fetchall()
                if rs:
                    for r in rs:
                        cur.execute("SELECT id from thaddeus_photo where photoalbum_id = ?", r)
                        res = cur.fetchone()
                        if res:
                            upd = res[0], r[0]
                            cur.execute("UPDATE thaddeus_photoalbum set coverpic_id = ? where id = ?", (upd))
                #DELETE any empty photoalbum
                cur = con.cursor()
                cur.execute("DELETE FROM thaddeus_photoalbum WHERE thaddeus_photoalbum.id NOT IN (SELECT thaddeus_photo.photoalbum_id FROM thaddeus_photo)")
            
        # Error Handling            
        except lite.Error, e:
            
            print "Error %s:" % e.args[0]
            sys.exit(1)
        

    
        # Always close DB connection    
        finally:
            
            if con:
                con.close()