#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
import sys, os

class Command(BaseCommand):
    args = '<path> usually MEDIA_ROOT'
    help = 'Delete all .thumbnails files under <path>'

    def handle(self, *args, **options):
        path = args[0]

        #try:

        dir = path
        
        # Return all files in dir, and all its subdirectories, ending in pattern
        def gen_files(dir, pattern):
            for dirname, subdirs, files in os.walk(dir):
                for f in files:
                    if f.endswith(pattern):
                        yield os.path.join(dirname, f)


        # Remove all files in the current dir matching ext
        for f in gen_files('.', '.thumbnail'):
            os.remove(f)
        
        # Error Handling            
        #except lite.Error, e:
            
        #    print "Error %s:" % e.args[0]
        #    sys.exit(1)