from django.test import TestCase
from django.conf import settings
from thaddeus.models import Photo,PhotoAlbum
import os, inspect
from zipfile import ZipFile as zip

#Front-End
class PhotoAlbumViewsTestCase(TestCase):
    def test_index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

#Management Commands
from django.core import management
from django.core.management import call_command

class DeleteThumbnailsTestCase(TestCase):
    def test_index(self):
        #Define some variables
        rpath=settings.MEDIA_ROOT
        testdir = rpath+'thaddeustest/'
        action = 'rotate'
              
        #Create Sample File System under MEDIA_ROOT for testing purposes - thaddeustest/default.jpg
        content = rpath+'../thaddeus/unittest/testfiles.zip'
        z = zip(content)
        for f in z.namelist():
            z.extract(f, testdir)
        
        #Call command file passing test path parameter
        call_command("deletethumbnails", testdir)   
        
        #Test Complete, Delete test files and directory
        os.remove(testdir+'photo2/images02.jpeg')
        os.removedirs(testdir+'photo2/')
        
class BatchInsertTestCase(TestCase):
       
    def test_index(self):
        #Define some variables
        rpath=settings.MEDIA_ROOT
        testdir = rpath+'thaddeustest/'
        
        #Create Sample File System under MEDIA_ROOT for testing purposes - thaddeustest/default.jpg
        content = rpath+'../thaddeus/unittest/testfiles.zip'
        print content
        z = zip(content)
        for f in z.namelist():
            z.extract(f, rpath+'thaddeustest/')
        
        #Call command file passing test path parameter
        call_command("batchinsert", rpath+'thaddeustest/')
        
        #Check Database to ensure batchinsert process a) create entry for thaddeustest photoalbum and default.jpg photo, b) Linked them correctly
        print PhotoAlbum.objects.filter(title='thaddeustest') 
        print Photo.objects.filter(image='thaddeustest/default.jpg')        
        
        #Test Complete, Delete test files and directory
        os.remove(testdir+'photo2/images02.jpeg')
        os.remove(testdir+'photo2/images02.thumbnail')
        os.remove(testdir+'photo2/thaddeus.thumbnail')
        os.removedirs(testdir+'photo2/')
