from django.conf.urls.defaults import *
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from thaddeus.models import Photo, PhotoAlbum
from thaddeus import views
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #url(r'^$', ListView.as_view(model=PhotoAlbum,context_object_name='thaddeus_photoalbum_list')),
    url(r'^$', views.photoAlbumListView, name='thaddeus_photoalbum_list'),
    url(r'^(?P<photoalbum_id>\d+)/photo_list/$', views.photoListView, name='thaddeus_photo_list'),
    url(r'^(?P<photoalbum_id>\d+)/photo_display/(?P<photo_id>\d+)/$', views.photoCarouselDisplayView, name='thaddeus_photo_carousel'),
    url(r'^(?P<photoalbum_id>\d+)/photo_carousel/$', views.photoCarouselView, name='thaddeus_photo_carousel'),
    url(r'^(?P<photoalbum_id>d+)/$', DetailView.as_view(model=Photo,context_object_name='thaddeus_photo_detail')),
    url(r'^flush/', views.flushDatabaseView),
    url(r'^batch/', views.runBatchView),
    url(r'^(?P<photoalbum_id>\d+)/deletepic/(?P<photo_id>\d+)/$', views.deletePicView),
    url(r'^(?P<photoalbum_id>\d+)/removepic/(?P<photo_id>\d+)/$', views.removePicView),
    url(r'^(?P<photoalbum_id>\d+)/rotatepic/(?P<photo_id>\d+)/$', views.rotatePicView),
    url(r'^(?P<photoalbum_id>\d+)/rotatepicleft/(?P<photo_id>\d+)/$', views.rotatePicLeftView),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    # For static files (CSS, JS...)
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
