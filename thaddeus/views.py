# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect,Http404
from thaddeus.models import Photo, PhotoAlbum
#from django.template import Context, loader
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.views import generic
from django.core import management
from subprocess import call
from django.db import connection
from django.conf import settings

def photoAlbumListView (request):
    thaddeus_photoalbum_list = PhotoAlbum.objects.all()
    return render(request, 'thaddeus/photoalbum_list.html', {'thaddeus_photoalbum_list': thaddeus_photoalbum_list})
        
def photoListView(request, photoalbum_id):
    thaddeus_photo_list = Photo.objects.filter(photoalbum=photoalbum_id)
    return render(request, 'thaddeus/photo_list.html', {'thaddeus_photo_list': thaddeus_photo_list, 'photoalbum_id': photoalbum_id})

def photoCarouselView(request, photoalbum_id):
    thaddeus_photo_list = Photo.objects.filter(photoalbum=photoalbum_id)
    return render(request, 'thaddeus/photoalbum_carousel.html', {'thaddeus_photo_list': thaddeus_photo_list})

def photoCarouselDisplayView(request, photoalbum_id, photo_id):
    thaddeus_photo_list = Photo.objects.filter(id=photo_id)        
    return render(request, 'thaddeus/photoalbum_carousel.html', {'thaddeus_photo_list': thaddeus_photo_list, 'photoalbum_id': photoalbum_id})
    
def flushDatabaseView(request):
    #Flush all data
    management.call_command('flush', verbosity=0, interactive=False)
    #Delete thumbnails
    management.call_command('deletethumbnails', settings.MEDIA_ROOT)
    # Need to reinsert dummy admin to be able to log into admin console
    management.call_command('createsuperuser', username='admin', email='mcornille@gmail.com')
    
    return HttpResponseRedirect('/')

def runBatchView(request):
    management.call_command('batchinsert')
    
    return HttpResponseRedirect('/')

def removePicView(request, photo_id, photoalbum_id):
    #management.call_command('processpic', 'remove', photo_id)
    management.call_command('processpic', settings.FILE_STORAGE['default']['PATH'], 'remove', photo_id)
    
    return HttpResponseRedirect('/%s/photo_list/' % photoalbum_id)

def deletePicView(request, photo_id, photoalbum_id):
    
    #management.call_command('processpic', 'delete', photo_id)
    management.call_command('processpic', settings.FILE_STORAGE['default']['PATH'], 'delete', photo_id)
    
    return HttpResponseRedirect('/%s/photo_list/' % photoalbum_id)

def rotatePicView(request, photo_id, photoalbum_id):
    
    #management.call_command('processpic', 'rotate', int(photo_id))
    management.call_command('processpic', settings.FILE_STORAGE['default']['PATH'], 'rotate', photo_id)
    
    return HttpResponseRedirect('/%s/photo_list/' % photoalbum_id)

def rotatePicLeftView(request, photo_id, photoalbum_id):
    
    #management.call_command('processpic', 'rotateleft', int(photo_id))
    management.call_command('processpic', settings.FILE_STORAGE['default']['PATH'], 'rotateleft', photo_id)
    
    return HttpResponseRedirect('/%s/photo_list/' % photoalbum_id)