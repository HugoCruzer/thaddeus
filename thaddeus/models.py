import datetime, os, sys
from PIL import Image
from django.core.files import File
from django.conf import settings
from django.db import models
from markdown import markdown
from django.conf import settings
#from filestorage import DatabaseStorage

class PhotoAlbum(models.Model):
    title = models.CharField(max_length=250, help_text='Maximum 250 characters.')
    summary = models.TextField(blank=True, help_text='An optional summary.')
    coverpic = models.ForeignKey('Photo', blank=True, related_name = '%(app_label)s_%(class)s_ownership')
    
    def __unicode__(self):
        return self.title
    def get_absolute_url(self):
        return settings.SITE_URL 
    
class Photo(models.Model):
    photoalbum = models.ForeignKey(PhotoAlbum)
    thumb = models.ImageField(upload_to='photos', editable=False)
    image = models.ImageField(upload_to='photos', help_text='Maximum resolution 800x600. Larger images will be resized.')
    title = models.CharField(max_length=250, help_text='Maximum 250 characters.')
    summary = models.TextField(blank=True, help_text='An optional summary.')
    summary_html = models.TextField(editable=False, blank=True)
    date = models.DateTimeField(default=datetime.datetime.now)
    #thumb = models.ImageField(upload_to='photos', editable=False, storage=DatabaseStorage())
    

    class Meta:
        ordering = ['-date']
    def __unicode__(self):
        return self.title
    def save(self, force_insert=False, force_update=False):
        if self.summary:
            self.summary_html = markdown(self.summary)
        super(Photo, self).save(force_insert, force_update)
        if self.image and not self.thumb:
            # Set the thumbnail size and maximum size.
            t_size = 200, 150
            max_size = 800, 600
            # Open the image that was uploaded.
            im = Image.open(settings.MEDIA_ROOT + str(self.image))
            # Compare the image size against the maximum size. If it is greater, the image will be resized.
            if im.size > max_size:
                # Using 'thumbnail', instead of 'resize', keeps the aspect ratio of the image.
                resize = im.thumbnail(max_size)
                resize.save(settings.MEDIA_ROOT + str(self.image), "JPEG")
                # Create the thumbnail and save the path to the database.
            im.thumbnail(t_size)
            im.save(settings.MEDIA_ROOT + os.path.splitext(str(self.image))[0] + ".thumbnail", "JPEG")
            self.thumb = os.path.splitext(str(self.image))[0] + ".thumbnail"
            super(Photo, self).save(force_insert, force_update)
    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return settings.SITE_URL+reverse('thaddeus.views.photoCarouselDisplayView', args=[self.photoalbum.id,self.id])
#    def get_by_photoalbum(self):
#        return self.model._default_manager.all()
