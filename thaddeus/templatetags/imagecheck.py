from django import template
from django.core.files.storage import default_storage
from django.conf import settings
import datetime, os, sys
from PIL import Image

register = template.Library()

@register.filter
def imageexists(imagepath): # Only one argument.
    """Checks image path does exist. Returns path as is if does, returns path to default file instead if not"""
    try:
        im = Image.open(imagepath)
        return imagepath
    except:
        return "../static/thaddeus/system/thaddeus.thumbnail"