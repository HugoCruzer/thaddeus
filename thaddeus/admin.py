from django.contrib import admin
from thaddeus.models import Photo, PhotoAlbum

#class PhotoAdmin(admin.ModelAdmin):
#	prepopulated_fields = { 'slug': ['title'] }
#	list_display = ('title', 'date')

#admin.site.register(Photo, PhotoAdmin)
admin.site.register(PhotoAlbum)
admin.site.register(Photo)

